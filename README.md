# Ansible | Utilisation des commandes ad-hoc avec Ansible


_______

Firstname : Carlin

Surname : FONGANG

Email : fongangcarlin@gmail.com

><img src="https://media.licdn.com/dms/image/C4E03AQEUnPkOFFTrWQ/profile-displayphoto-shrink_400_400/0/1618084678051?e=1710979200&v=beta&t=sMjRKoI0WFlbqYYgN0TWVobs9k31DBeSiOffAOM8HAo" width="50" height="50" alt="Carlin Fongang"> 
>
>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://githut.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

## Contexte
Dans ce lab, nous allons procéder à l'installation d'Ansible sur Ubuntu, sur une instance cible créée sur AWS.


## Prérequis
Avoir une machine avec Ubuntu déjà installé.

Dans notre cas, nous allons provisionner une instance EC2 s'exécutant sous Ubuntu via AWS, sur laquelle nous effectuerons toutes nos configurations.

Documentation complémentaire.
[Doc ansible](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/yum_module.html)
[lancer une instance ec2 sur aws à l'aide de terraform](https://gitlab.com/CarlinFongang-Labs/Terraform/lab2-terraform-aws)


## Objectifs

Nous créons un cluster, c'est-à-dire une machine hôte hébergeant Ansible et un client cible qui servira d'instance pour la configuration.

Nous créerons un fichier d'inventaire nommé "hosts".

Nous utiliserons une commande ad hoc pour pinger via Ansible.

Nous créerons un fichier "test.txt" contenant "hello aCloud.Digital", qui sera stocké dans le répertoire "/home/ubuntu/test.txt".

Nous vérifierons que le fichier a bien été créé avec le contenu souhaité.

Nous ajouterons un client et modifierons le fichier d'inventaire pour inclure le nouveau client.

Nous relancerons l'action de ping et de création de fichier sur les deux instances et vérifierons les résultats.

Enfin, nous testerons l'effet du module "setup" sur l'inventaire.

## Prérequis
Pour des raisons de facilité, nous utilisons la même clé SSH pour la connexion à la machine hôte Ansible et à la machine cliente.

## 1. Création d'un cluster d'instances EC2
[Lancer une instance EC2 sur AWS à l'aide de Terraform](https://gitlab.com/CarlinFongang-Labs/Terraform/lab2-terraform-aws)

## 2. Création du fichier d'inventaire hosts 
### 2.1. Connexion à l'instance hôte et client
```bash
ssh -i devops-aCD.pem ubuntu@public_ip_address_ansible
```

>![alt text](img/image-1.png)

Vérification de la version d'Ansible

>![alt text](img/image-3.png)
*Ansible est bien installé sur la machine hôte*

### 2.3. Connexion au client

```bash
ssh -i devops-aCD.pem ubuntu@public_ip_address_client
```

>![alt text](img/image-2.png)

>![alt text](img/image-4.png)
*Ansible n'est pas installé sur la machine cliente*

### 2.4. Réalisation de l'inventaire Ansible

### Méthode 1 : Création du fichier hosts avec les informations d'identification écrites en dur

1. Une fois connecté à la machine hôte, nous créons un fichier **hosts** :

```bash
nano hosts
```

Dans le fichier hosts ouvert, nous allons y ajouter **l'adresse IP** de la machine cible ainsi que le nom d'utilisateur : 
cette méthode permet de se connecter à la machine cliente lorsque le mot de passe de celle-ci est connu.

```bash 
[clients]
3.225.69.88 ansible_user=ubuntu ansible_password=admin
```

>![alt text](img/image-5.png)

2. Ajout de la machine cliente aux cibles de confiance d'Ansible :

```bash
nano hosts
```

```bash
[clients]
3.225.69.88 ansible_user=ubuntu ansible_password=admin ansible_ssh_common_args='-o StrictHostKeyChecking=no'
```

L'ajout de cette dernière ligne permet de désactiver temporairement la vérification de la clé de l'hôte pour Ansible.

On enregistre le fichier d'inventaire.

>![alt text](img/image-6.png)

### Méthode 2 : Création du fichier d'inventaire et usage de la paire de clés SSH
1. Création d'un répertoire caché et copie de la paire de clés : 

Depuis le terminal de la machine hôte : 

```bash 
mkdir .secret
nano .secret/devops-aCD.pem
```
Dans le fichier devops-aCD.pem ouvert, on y copie le contenu de la clé générée lors de la création de l'instance EC2.

>![alt text](img/image-7.png)

2. Création du fichier hosts

```bash
nano hosts
```

À l'intérieur de ce fichier hosts, on y mettra : 

```bash
[clients]
44.207.235.103 ansible_user=ubuntu ansible_ssh_private_key_file=.secret/devops-aCD.pem
```
**.secret/devops-aCD.pem** représente le chemin de la paire de clés SSH, qui est la même clé générée lors de la création de l'instance EC2 cliente.

## 3. Utilisation de la commande ad-hoc pour pinger la machine cliente à l'aide d'Ansible.

```bash
ansible -i hosts all -m ping
```

Le résultat en sortie console :

>![alt text](img/image-8.png)
*Réponse de la machine distante **client***

## 4. Création du fichier "test.txt" ("hello aCloud.Digital"), qui sera stocké dans le répertoire "/home/ubuntu/test.txt" du client

```bash
ansible -i hosts all -m copy -a "dest=/home/ubuntu/test.txt content='hello aCloud.Digital'"
```

>![alt text](img/image-9.png)
*Sortie console après création du fichier vers la machine cible*

## 5. Vérification de la création du fichier
Pour vérifier cela, nous allons nous connecter à la machine cliente et lister les fichiers présents dans le répertoire **/home/ubuntu**

```bash
ssh -i devops-aCD.pem ubuntu@public_ip_address_client
ls
```
>![alt text](img/image-10.png)

## 6. Création d'un second client et mise à jour du fichier d'inventaire 

1. Création d'une nouvelle instance
[Lancer une instance EC2 sur AWS à l'aide de Terraform](https://gitlab.com/CarlinFongang-Labs/Terraform/lab2-terraform-aws)

>![alt text](img/image-11.png)
*Client 2 est créé*

2. Mise à jour de l'inventaire
On va de nouveau éditer le fichier **hosts** depuis la machine hôte et y rajouter les informations de la seconde machine cliente

```bash
nano hosts
```
```bash
[clients]
44.207.235.103 ansible_user=ubuntu ansible_ssh_private_key_file=.secret/devops-aCD.pem
44.210.206.155 ansible_user=ubuntu ansible_ssh_private_key_file=.secret/devops-aCD.pem
```

>![alt text](img/image-12.png)
*Ajout des informations de la seconde machine au fichier d'inventaire*

## 7. Relance de l'action de "ping", création du fichier "test2.txt" sur les deux instances et vérification du résultat

1. Relance du ping

```bash
ansible -i hosts all -m ping
```

>![alt text](img/image-13.png)
*Réponse du ping de toutes les machines de l'inventaire*

2. Création du fichier **test2.txt** avec pour contenu **"hello aCloud.Digital"**

```bash
ansible -i hosts all -m copy -a "dest=/home/ubuntu/test2.txt content='hello aCloud.Digital'"
```

>![alt text](img/image-14.png)
*Création du fichier **test2.txt** sur l'ensemble de l'inventaire*

3. Vérification de la présence du fichier sur chacune des machines clientes

Client 1 : 

>![alt text](img/image-15.png)
*Le fichier **test2.txt** a été créé sur le client 1*

Client 2 : 

>![alt text](img/image-16.png)
*Le fichier **test2.txt** a été créé sur le client 2*

## 8. Test du module "setup" sur l'inventaire

Depuis la machine hôte Ansible, nous allons entrer la commande :

```bash
ansible -i hosts all -m setup
```

>![alt text](img/image-17.png)
*Sortie console après utilisation du module **setup***


**Explication**

La commande `ansible -i hosts all -m setup` utilise le module `setup` d'Ansible pour collecter et afficher des informations détaillées sur la configuration de toutes les machines cibles listées dans le fichier d'inventaire `hosts`. Le module `setup` récupère automatiquement les faits système tels que le système d'exploitation, le réseau, le matériel et d'autres détails techniques de chaque hôte.